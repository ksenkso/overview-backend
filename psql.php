<?php

define( 'PERIOD', 3600 );

/**
 * @const bool DEVELOPMENT
 */
define( 'DEVELOPMENT', true );

function period()
{
    return time() - PERIOD;
}

/**
 * @param string $db
 * @param string $user
 * @param string $password
 * @param string $host
 * @return bool|resource
 */
function p_connect($db = 'zabbix', $user = 'zabbix', $password = '812KKK', $host = 'localhost')
{
    $connect_str = "host=$host ";
    $connect_str .= "port=5432 ";
    $connect_str .= "dbname=$db ";
    $connect_str .= "user=$user ";
    $connect_str .= "password=$password ";
    $connect_str .= "sslmode=verify-full ";
    $connect_str .= "sslcert=/usr/lib/ssl/certs/dashboard/postgresql.crt ";
    $connect_str .= "sslkey=/usr/lib/ssl/certs/dashboard/postgresql.key ";
    $connect_str .= "sslrootcert=/usr/lib/ssl/certs/dashboard/root.crt";
    $pg_con = pg_connect( $connect_str );
    return $pg_con ? $pg_con : false;
}

/**
 * @param resource $conn Connection to PSQL database
 * @param string $q_string Query string
 * @param array $params Query parameters
 * @param string $q_name
 * @return resource
 */
function p_query($conn, $q_string, $params = array(), $q_name = "")
{
    $q = pg_prepare( $conn, $q_name, $q_string ) or die("PREPARE");
    $res = pg_execute( $conn, $q_name, $params ) or die("EXECUTE");
    //echo pg_result_error($res);
    return pg_fetch_all( $res );

}

/**
 * @param $c
 * @return array
 */
function get_cpu($c)
{
    $cpu = array('load' => array(), 'time' => array());

    $cpu_time = p_query(
        $c,
        "SELECT DISTINCT h.name, 100 - AVG(hst.value) OVER (PARTITION BY i.itemid) AS cpu_time 
				FROM history hst 
				LEFT JOIN items i ON i.itemid=hst.itemid 
				LEFT JOIN hosts h ON h.hostid=i.hostid 
				WHERE hst.itemid IN (
				SELECT itemid FROM items WHERE name = $1
				) AND hst.clock BETWEEN $2 AND $3",
        array('(CPU) Time: Idle (Total)', period(), time())
    );
    $cpu_load = p_query(
        $c,
        "SELECT DISTINCT h.name, AVG(hst.value) OVER (PARTITION BY i.itemid) AS cpu_load
				FROM history hst 
				LEFT JOIN items i ON i.itemid=hst.itemid 
				LEFT JOIN hosts h ON h.hostid=i.hostid 
				WHERE hst.itemid IN (
				SELECT itemid FROM items 
				WHERE name = $1
				) AND hst.clock BETWEEN $2 AND $3;",
        array('(CPU) Processor load (15 min average per core)', period(), time())
    );

    $res = array(
        'time' => array(
            '0' => array(),
            '1' => array(),
            '2' => array(),
            '3' => array()
        ),
        'load' => array(
            '0' => array(),
            '1' => array(),
            '2' => array(),
            '3' => array()
        )
    );

    if ($cpu_load && $cpu_time) {

        $cpu['time'] = $cpu_time;
        $cpu['load'] = $cpu_load;


        foreach ($cpu['time'] as $key => $item) {
            switch (true) {
                case $item['cpu_time'] < 25:
                    array_push( $res['time']['0'], $item['name'] );
                    break;
                case $item['cpu_time'] < 50:
                    array_push( $res['time']['1'], $item['name'] );
                    break;
                case $item['cpu_time'] < 75:
                    array_push( $res['time']['2'], $item['name'] );
                    break;
                default:
                    array_push( $res['time']['3'], $item['name'] );
                    break;
            }
        }

        foreach ($cpu['load'] as $key => $item) {
            switch (true) {
                case $item['cpu_load'] < 25:
                    array_push( $res['load']['0'], $item['name'] );
                    break;
                case $item['cpu_load'] < 50:
                    array_push( $res['load']['1'], $item['name'] );
                    break;
                case $item['cpu_load'] < 75:
                    array_push( $res['load']['2'], $item['name'] );
                    break;
                default:
                    array_push( $res['load']['3'], $item['name'] );
                    break;
            }
        }
    }
    return $res;
}

function get_memory($c)
{
    
    $swap_q = "SELECT  1 - mem.mem_free / total.lastvalue::numeric as swap,
					mem.name from (
						select distinct avg(hst.value) over (partition by hst.itemid) as mem_free,
						h.name as name,
						i.hostid as hid,
						i.itemid as iid
						from history_uint hst 
						left join items i on hst.itemid=i.itemid
						left join hosts h on h.hostid=i.hostid
						where i.itemid in (
							select itemid from items 
							where name = '(MEM) Free swap space' and templateid is not null
						) and hst.clock between extract(epoch from now())::bigint - 3600 AND extract(epoch from now())::bigint
						) as mem left join (
							select lastvalue, itemid, hostid from items 
							where name ='(MEM) Total swap space' and templateid is not null
						) as total
					on mem.hid=total.hostid;";

    $ram_q = "SELECT 1 - mem.mem_free / to_number(total.lastvalue, '999999999999') as mem,
					mem.name from (
					select distinct avg(hst.value) over (partition by hst.itemid) as mem_free,
					h.name as name,
					i.itemid as iid
					from history_uint hst 
					left join items i on hst.itemid=i.itemid
					left join hosts h on h.hostid=i.hostid
					where i.itemid in (
						select itemid from items 
						where name = '(MEM) Free memory'
					) and hst.clock between extract(epoch from now())::bigint - 3600 AND extract(epoch from now())::bigint
					) as mem  left join (
					select lastvalue, itemid from items 
					where name = '(MEM) Total memory'
					) as total on mem.iid+1=total.itemid;";
    $mem_ram = p_query( $c, $ram_q );
    $mem_swap = p_query( $c, $swap_q );

    $res = array(
        'ram' => array(
            '0' => array(),
            '1' => array(),
            '2' => array(),
            '3' => array()
        ),
        'swap' => array(
            '0' => array(),
            '1' => array(),
            '2' => array(),
            '3' => array()
        )
    );

    if ($mem_ram && $mem_swap) {
        $mem = array('ram' => $mem_ram, 'swap' => $mem_swap);

        foreach ($mem['ram'] as $key => $item) {
            switch (true) {
                case $item['mem'] < 0.25:
                    array_push( $res['ram']['0'], $item['name'] );
                    break;
                case $item['mem'] < 0.50:
                    array_push( $res['ram']['1'], $item['name'] );
                    break;
                case $item['mem'] < 0.75:
                    array_push( $res['ram']['2'], $item['name'] );
                    break;
                default:
                    array_push( $res['ram']['3'], $item['name'] );
                    break;
            }
        }

        foreach ($mem['swap'] as $key => $item) {
            switch (true) {
                case $item['swap'] < 0.25:
                    array_push( $res['swap']['0'], $item['name'] );
                    break;
                case $item['swap'] < 0.50:
                    array_push( $res['swap']['1'], $item['name'] );
                    break;
                case $item['swap'] < 0.75:
                    array_push( $res['swap']['2'], $item['name'] );
                    break;
                default:
                    array_push( $res['swap']['3'], $item['name'] );
                    break;
            }
        }
    }
    return $res;
}

function get_disk($c)
{
    $disk = array('usage' => array(), 'time' => array());

    $disk_usage = p_query(
        $c,
        "SELECT (win_used.value / win_total.value) * 100 as disk_usage, win_used.hname as name from (
				select sum(avg_used.value) as value, avg_used.hid as hid, avg_used.name as hname from (
					select distinct i.itemid, i.hostid, avg(h.value) over (partition by i.itemid) as value,
						hs.name as name,
						hs.hostid as hid,
						i.key_ 
					from
						items i
						left join history_uint h on i.itemid = h.itemid
						left join hosts hs on i.hostid=hs.hostid
					where 
						i.name = 'Used disk space on $1'
						and lastvalue is not null
						and key_ like 'vfs\.fs\.size%\:%'
						and h.clock between $1 AND $2
					) as avg_used
					group by avg_used.hid, avg_used.name
				) as win_used
			left join (
				select distinct on (sum(lastvalue::bigint) over (partition by key_ like 'vfs\.fs\.size%\:%')) 
					sum(lastvalue::bigint) over (partition by (key_ like 'vfs\.fs\.size%\:%')) as value,
					hostid
				from
					items 
				where 
					name = 'Total disk space on $1'
					and lastvalue is not null
					and key_ like 'vfs\.fs\.size%\:%'
			) as win_total
			on win_used.hid=win_total.hostid
			UNION
			-- linux disk usage
			-- linux disk total
			select 
				(linux_used.value / linux_total.value::bigint) * 100 as disk_usage,
				linux_used.hname as name
			from (
			SELECT DISTINCT
				h.name, AVG(hst.value) over (partition by i.itemid) as value,
				h.hostid as hid,
				h.name as hname
			FROM 
				history_uint hst 
				left join items i on i.itemid=hst.itemid 
				left join hosts h on h.hostid=i.hostid 
			WHERE 
				hst.itemid in (
					select itemid
					from items 
					where name = 'Used disk space on $1' and lastvalue is not null and key_ like 'vfs\.fs\.size%/\,%'
				) 
				and hst.clock between $1 AND $2
			) as linux_used
			left join (
			-- linux disk used	
			select
				lastvalue as value, hostid as hid
			from
				items
			where
				name = 'Total disk space on $1'
				and lastvalue is not null
				and key_ like 'vfs\.fs\.size%/\,%'
			) as linux_total
			on linux_used.hid = linux_total.hid;",
        array(period(), time())
    );

    $disk_time = p_query(
        $c,
        "SELECT distinct avg(100 - hst.value) over (partition by hst.itemid) as disk_time,
			h.name
			from history hst
			left join items i on i.itemid=hst.itemid
			left join hosts h on i.hostid=h.hostid
			where hst.itemid in (
				select itemid
				from items
				where key_ in ($1)
			) and hst.clock between $2 AND $3;",
        array('perf_counter[\\234(_Total)\\1746]', period(), time())
    );

    $res = array(
        'time' => array(
            '0' => array(),
            '1' => array(),
            '2' => array(),
            '3' => array()
        ),
        'usage' => array(
            '0' => array(),
            '1' => array(),
            '2' => array(),
            '3' => array()
        )
    );

    if ($disk_time && $disk_usage) {

        $disk['time'] = $disk_time;
        $disk['usage'] = $disk_usage;


        foreach ($disk['time'] as $key => $item) {
            switch (true) {
                case $item['disk_time'] < 25:
                    array_push( $res['time']['0'], $item['name'] );
                    break;
                case $item['disk_time'] < 50:
                    array_push( $res['time']['1'], $item['name'] );
                    break;
                case $item['disk_time'] < 75:
                    array_push( $res['time']['2'], $item['name'] );
                    break;
                default:
                    array_push( $res['time']['3'], $item['name'] );
                    break;
            }
        }

        foreach ($disk['usage'] as $key => $item) {
            switch (true) {
                case $item['disk_usage'] < 25:
                    array_push( $res['usage']['0'], $item['name'] );
                    break;
                case $item['disk_usage'] < 50:
                    array_push( $res['usage']['1'], $item['name'] );
                    break;
                case $item['disk_usage'] < 75:
                    array_push( $res['usage']['2'], $item['name'] );
                    break;
                default:
                    array_push( $res['usage']['3'], $item['name'] );
                    break;
            }
        }
    }
    return $res;
}

/**
 * @param resource $c
 * @param string $name
 * @return bool
 */

function get_availability($c, $name)
{

    $name_map = array(
        'Servers' => array('{4}'),/*9*/
        'Network Devices' => array('{5}'),/*8*/
        'Sensors' => array('{1}')/*21*/
    );
    
    $availability = p_query(
        $c,
        "SELECT 
				result.gn as name,
				coalesce(nullif(result.availability::char, ''), '0')::numeric as percent,
				coalesce(nullif(result.available::char, ''), '0')::numeric as available,
				result.total
			from (
				select 
					round(a.available::numeric / t.total::numeric, 2) as availability,
					a.available::numeric as available,
					t.total::numeric as total,
					t.group_name as gn
				from (
					select distinct
						count(h.available) over (partition by g.groupid) as total,
						g.name as group_name
					from
						hosts h
						left join hosts_groups hg on h.hostid=hg.hostid
						left join groups g on g.groupid=hg.groupid
					where 
						g.groupid = any($1::int[])
				) as t
				left join (
					select distinct
						count(h.available) over (partition by g.groupid) as available,
						g.name as group_name
					from
						hosts h
						left join hosts_groups hg on h.hostid=hg.hostid
						left join groups g on g.groupid=hg.groupid
					where
						h.available=1 and g.groupid = any($1::int[])
				) as a
				on t.group_name=a.group_name
			) as result;",
        $name_map[$name]
    );
    //echo json_encode($availability)."\n";
    if ($availability && $availability['0']) {
        $availability['0']['name'] = $name;
        return $availability['0'];
    } else {
        return false;
    }

}

function get_sensors($c)
{

    $sensors_params =  DEVELOPMENT
        ? array(
            'fire' => 'Free disk space is less than 20% on volume C:',
            'flood' => 'Free disk space is less than 20% on volume D:',
            'temp_low' => 'Free disk space is less than 20% on volume D:',
            'temp_high' => 'Free disk space is less than 20% on volume D:',
            'hum_low' => 'Free disk space is less than 20% on volume C:',
            'hum_high' => 'Free disk space is less than 20% on volume D:'
        )
        : array(
            'fire' => 'Server room: Fire Alarm',
            'flood' => 'Server Room: Flood Alarm',
            'temp_low' => 'Server Room: Temperature Low',
            'temp_high' => 'Server Room: Temperature High',
            'hum_low' => 'Server Room: Humidity Low',
            'hum_high' => 'Server Room: Humidity High'
        );
    $sensors_query ="select * from 
                    ( select C.value as fire
                    from triggers as C
                    where C.description = $1 ) as CC, 
                    ( select D.value as flood
                    from triggers as D
                    where D.description = $2 ) as DD, 
                    ( select F.value as temp_low
                    from triggers as F
                    where F.description = $3 ) as FF,
                    ( select E.value as temp_high
                    from triggers as E
                    where E.description = $4 ) as EE,
                    ( select G.value as hum_low
                    from triggers as G
                    where G.description = $5 ) as GG, 
                    ( select H.value as hum_high
                    from triggers as H
                    where H.description = $6 ) as HH";
    $sensors = p_query(
        $c,
        $sensors_query,
        array(
            $sensors_params['fire'],
            $sensors_params['flood'],
            $sensors_params['temp_low'],
            $sensors_params['temp_high'],
            $sensors_params['hum_low'],
            $sensors_params['hum_high']
        ));

    $sensors = $sensors['0'];
    foreach ($sensors as $key => $value)
    {
        $sensors[$key] = $value ? $value : false;
    }
    $sensors['temperature'] = $sensors['temp_low'] || $sensors['temp_high']
        ? array('low' => $sensors['temp_low'], 'high' => $sensors['temp_high']) 
        : false;
    $sensors['humidity'] = $sensors['hum_low'] || $sensors['hum_high']
        ? array('low' => $sensors['hum_low'], 'high' => $sensors['hum_high'])
        : false;
    unset($sensors['temp_low'], $sensors['temp_high'], $sensors['hum_low'],$sensors['hum_high']);
    return $sensors;
}