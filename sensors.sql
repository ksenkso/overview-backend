﻿select * from (
select 
	C.value as FloodHigh
from 
	triggers as C
where
	C.description = 'Free disk space is less than 20% on volume C:'

) as CC cross join (
select 
	D.value as FloodLow
from 
	triggers as D
where
	D.description = 'Free disk space is less than 20% on volume D:'

) as DD cross join (
select 
	F.value as TempHigh
from 
	triggers as F
where
	F.description = 'Free disk space is less than 20% on volume D:'

) as FF 
