<?php

include 'psql.php';
include 'auth.php';

/** @var array $graphs_tables_mapping */
$graphs_tables_mapping = array(
    'mem_r' => 'memory_ram',
    'mem_s' => 'memory_swap',
    'cpu_t' => 'cpu_time',
    'cpu_l' => 'cpu_load',
    'dsk_u' => 'disk_usage'
);

/**
 * @param $type
 * @param $period
 * @return string
 */
function form_query_string($type, $period)
{
    if (strpos($type, 'avail') === false) {
        if ($period < 4) {
            return 'SELECT g0 as "0", g1 as "1", g2 as "2", g3 as "3" from ' . $type . ' order by id desc limit 10';
        } else {
            return 'SELECT 	avg(g0) as "0", avg(g1) as "1", avg(g2) as "2", avg(g3) as "3"
				from (select row_number() over (order by id desc) as seqnum, t.*
				      from ' . $type . ' t
				      limit	' . $period * 10 . '
				     ) t
				group by ((seqnum - 1) / ' . $period . ')
				order by 1';
        }
    } else {
        if ($type == 'avail_hardware') {
            return 'select servers, network_devices, sensors from avail_hardware order by id desc limit '. $period * 10;
        } else {
            return "select servers, network_devices, sensors from avail_services order by id desc limit ". $period * 10;
        }
    }
}

/**
 * @param resource $history_connection
 * @param string $type
 * @param int $period
 * @return array
 */
function get_group($history_connection, $type, $period = 1)
{
    $query_string = form_query_string( $type, $period );
    $result = p_query( $history_connection, $query_string );
    return $result;
}

/**
 * @return resource
 * @internal param $history_connection
 */
function handle_get_availability_history()
{
    $graph_period = 1;
    $graph_type = $_GET['type'];
    if (isset($_GET['period']) && !empty($_GET['period'])) {
        $graph_period = filter_input( INPUT_GET, 'period', FILTER_VALIDATE_INT );
    }

    $history_conn = p_connect( 'custom_history', 'history_updater', 'history' ); // third parameter - password for Postgres role
    $query_string = form_query_string( $graph_type, $graph_period );
    $result = p_query( $history_conn, $query_string );
    return array('avail_hardware' => $result);
}

/**
 * @return array|bool
 */
function handle_get_group()
{
    $graph_period = 1;
    $graph_type = $_GET['type'];
    if (isset($_GET['period']) && !empty($_GET['period'])) {
        $graph_period = filter_input( INPUT_GET, 'period', FILTER_VALIDATE_INT );
    }
    if ($graph_type) {
        $history_conn = p_connect( 'custom_history', 'history_updater', 'history' );
        $type_group = array();
        $type_group[$graph_type] = get_group( $history_conn, $graph_type, $graph_period );
        if ($type_group[$graph_type]) return $type_group;
    }
    return false;
}

/**
 * @param $graphs_tables_mapping
 * @return array
 */
function handle_get_all_groups($graphs_tables_mapping)
{
    if (isset($_GET['period']) && !empty($_GET['period'])) {
        $history_conn = p_connect( 'custom_history', 'history_updater', 'history' );
        $graph_period = filter_input( INPUT_GET, 'period', FILTER_VALIDATE_INT );
        $groups_total = array();
        foreach ($graphs_tables_mapping as $type => $table_name) {
            $type_groups = get_group( $history_conn, $table_name, $graph_period );
            $groups_total[$table_name] = $type_groups;
        }
        return $groups_total;
    } else return array('error' => 'ERROR');
}

if (check_auth_ldap()) {
    if (isset($_GET['type']) && $_GET['type'] === 'all') {
        $groups_total = handle_get_all_groups( $graphs_tables_mapping );
        if ($groups_total) {
            echo json_encode( $groups_total );
        } else {
            echo "ERRORN";
        }
    } else if (isset($_GET['type']) && !empty($_GET['type']) && $_GET['type'] != 'all' && $_GET['type'] != 'avail_hardware' && $_GET['type'] != 'avail_services') {
        $group = handle_get_group();
        if ($group) echo json_encode( $group );
    } else if (isset($_GET['type']) && !empty($_GET['type']) && ($_GET['type'] == 'avail_hardware' || $_GET['type'] == 'avail_services')) {
        $availability = handle_get_availability_history();
        if ($availability) echo json_encode($availability);
    }
}
