<?php

include 'psql.php';

$zabbix_db_conn = p_connect();



/**
 * @param $zabbix_connection
 * @param $history_connection
 * @return bool|mixed
 */
function write_availability($zabbix_connection, $history_connection)
{
    $avail_servers = get_availability( $zabbix_connection, 'Servers' );
    $avail_network = get_availability( $zabbix_connection, 'Network Devices' );
    $avail_sensors = get_availability( $zabbix_connection, 'Sensors' );
    if ($avail_servers && $avail_network && $avail_sensors) {
        $availability = array(
            'servers' => $avail_servers['percent'],
            'network_devices' => $avail_network['percent'],
            'sensors' => $avail_sensors['percent']
        );
        print_r( $availability );

        $insert = pg_insert( $history_connection, 'avail_hardware', $availability );
        return $insert;
    }
    return false;
}

/**
 * @param $history_connection
 * @param $zabbix_connection
 * @param $queries
 * @return bool|mixed
 */
function write_groups($history_connection, $zabbix_connection, $queries)
{
    $groups = array();
    $result = false;
    foreach ($queries as $table_name => $query) {
        $groups_partial = p_query( $zabbix_connection, $query ) or error_log( "ERROR WHEN GETTING GROUP PARTIALS FOR $table_name\n" );
        $groups[$table_name] = array();
        foreach ($groups_partial as $key => $row) {
            $groups[$table_name][$row['group']] = $row['count'];
        }
        $result = pg_insert( $history_connection, $table_name, $groups[$table_name] ) or error_log( "ERROR WHEN WRITING GROUP $table_name\n" );
    }
    return $result;
}

/**
 * This function write groups to memory_* tables. Functions named like `write_<type_of_value>_groups`
 * will write groups to corresponding tables in custom_history database.
 *
 * @param resource $zbx_conn Zabbix connection identifier
 * @param resource $hst_conn Custom history connection identifier
 * @return bool|mixed All inserts were done without errors
 */
function write_memory_groups($zbx_conn, $hst_conn)
{
    $swap_q = "SELECT distinct count(res.group) over (partition by res.group), res.group from(
				SELECT get_group((1 - mem.mem_free / total.lastvalue::numeric)*100) as group,
				mem.name from (
				select distinct avg(hst.value) over (partition by hst.itemid) as mem_free,
				h.name as name,
				i.hostid as hid,
				i.itemid as iid
				from history_uint hst 
				left join items i on hst.itemid=i.itemid
				left join hosts h on h.hostid=i.hostid
				where i.itemid in (
					select itemid from items 
					where name = '(MEM) Free swap space' and templateid is not null
				) and hst.clock between extract(epoch from now())::bigint - 3600 AND extract(epoch from now())::bigint
				) as mem  left join (
				select lastvalue, itemid, hostid from items 
				where name ='(MEM) Total swap space' and templateid is not null
				) as total on mem.hid=total.hostid) as res;";

    $ram_q = "SELECT distinct count(res.group) over (partition by res.group), res.group from(
				SELECT get_group((1 - mem.mem_free / to_number(total.lastvalue, '999999999999'))*100) as group,
				mem.name from (
				select distinct avg(hst.value) over (partition by hst.itemid) as mem_free,
				h.name as name,
				i.hostid as hid,
				i.itemid as iid
				from history_uint hst 
				left join items i on hst.itemid=i.itemid
				left join hosts h on h.hostid=i.hostid
				where i.itemid in (
					select itemid from items 
					where name = '(MEM) Free memory' and templateid is not null
				) and hst.clock between extract(epoch from now())::bigint - 3600 AND extract(epoch from now())::bigint
				) as mem  left join (
				select lastvalue, itemid, hostid from items 
				where name = '(MEM) Total memory' and templateid is not null
				) as total on mem.hid=total.hostid) as res order by res.group;";
    write_groups(
        $hst_conn,
        $zbx_conn,
        array(
            'memory_ram' => $ram_q,
            'memory_swap' => $swap_q
        )
    );
}

/**
 * @see write_memory_groups($zbx_conn, $hst_conn)
 * @param $zbx_conn
 * @param $hst_conn
 */
function write_cpu_groups($zbx_conn, $hst_conn)
{
    $time_q = "SELECT distinct count(res.group) over (partition by res.group), res.group from (
				SELECT DISTINCT h.name, get_group(100 - AVG(hst.value) OVER (PARTITION BY i.itemid)) AS group
				FROM history hst 
				LEFT JOIN items i ON i.itemid=hst.itemid 
				LEFT JOIN hosts h ON h.hostid=i.hostid 
				WHERE hst.itemid IN (
				SELECT itemid FROM items WHERE name ='(CPU) Time: Idle (Total)'
				) AND hst.clock BETWEEN extract(epoch from now())::bigint - 3600 AND extract(epoch from now())::bigint
				) as res;";

    $load_q = "SELECT distinct count(res.group) over (partition by res.group), res.group from (
				SELECT DISTINCT h.name, get_group(AVG(hst.value) OVER (PARTITION BY i.itemid)) AS group
				FROM history hst 
				LEFT JOIN items i ON i.itemid=hst.itemid 
				LEFT JOIN hosts h ON h.hostid=i.hostid 
				WHERE hst.itemid IN (
				SELECT itemid FROM items 
				WHERE name ='(CPU) Processor load (15 min average per core)'
				) AND hst.clock BETWEEN extract(epoch from now())::bigint - 3600 AND extract(epoch from now())::bigint
				) as res;";

    write_groups(
        $hst_conn,
        $zbx_conn,
        array(
            'cpu_time' => $time_q,
            'cpu_load' => $load_q
        )
    );
}

/**
 * @see write_memory_groups($zbx_conn, $hst_conn)
 * @param $zbx_conn
 * @param $hst_conn
 */
function write_disk_groups($zbx_conn, $hst_conn)
{

    $usage_q = "SELECT distinct count(res.disk_usage) over (partition by res.disk_usage), res.disk_usage as group from (
	SELECT get_group((win_used.value / win_total.value) * 100) as disk_usage, win_used.hname as name from (
		select sum(avg_used.value) as value, avg_used.hid as hid, avg_used.name as hname from (
			select distinct i.itemid, i.hostid, avg(h.value) over (partition by i.itemid) as value,
				hs.name as name,
				hs.hostid as hid,
				i.key_ 
			from
				items i
				left join history_uint h on i.itemid = h.itemid
				left join hosts hs on i.hostid=hs.hostid
			where 
				i.name = 'Used disk space on $1'
				and lastvalue is not null
				and key_ like 'vfs\.fs\.size%\:%'
				and h.clock between extract(epoch from now())::bigint - 3600 AND extract(epoch from now())::bigint
			) as avg_used
			group by avg_used.hid, avg_used.name
		) as win_used
	left join (
		select distinct on (sum(lastvalue::bigint) over (partition by key_ like 'vfs\.fs\.size%\:%')) 
			sum(lastvalue::bigint) over (partition by (key_ like 'vfs\.fs\.size%\:%')) as value,
			hostid
		from
			items 
		where 
			name = 'Total disk space on $1'
			and lastvalue is not null
			and key_ like 'vfs\.fs\.size%\:%'
	) as win_total
	on win_used.hid=win_total.hostid
	UNION
	-- linux disk usage
	-- linux disk total
	select 
		get_group((linux_used.value / linux_total.value::bigint) * 100) as disk_usage,
		linux_used.hname as name
	from (
	SELECT DISTINCT
		h.name, AVG(hst.value) over (partition by i.itemid) as value,
		h.hostid as hid,
		h.name as hname
	FROM 
		history_uint hst 
		left join items i on i.itemid=hst.itemid 
		left join hosts h on h.hostid=i.hostid 
	WHERE 
		hst.itemid in (
			select itemid
			from items 
			where name = 'Used disk space on $1' and lastvalue is not null and key_ like 'vfs\.fs\.size%/\,%'
		) 
		and hst.clock between extract(epoch from now())::bigint - 3600 AND extract(epoch from now())::bigint
	) as linux_used
	left join (
	-- linux disk used	
	select
		lastvalue as value, hostid as hid
	from
		items
	where
		name = 'Total disk space on $1'
		and lastvalue is not null
		and key_ like 'vfs\.fs\.size%/\,%'
	) as linux_total
	on linux_used.hid = linux_total.hid
	) as res;";

    write_groups(
        $hst_conn,
        $zbx_conn,
        array(
            'disk_usage' => $usage_q
        )
    );
}

$history_conn = p_connect( 'custom_history', 'history_updater', 'history' );
write_availability( $zabbix_db_conn, $history_conn );
write_memory_groups( $zabbix_db_conn, $history_conn );
write_cpu_groups( $zabbix_db_conn, $history_conn );
write_disk_groups( $zabbix_db_conn, $history_conn );
