<?php 

  function check_ldap($username, $pass) {
    $ldapServer = 'ldaps://ldap.test.local/';
    $ldapPort = 389;
    $dn = "CN=test user.,OU=loginer,DC=ldap,DC=test,DC=local";

    $conn = ldap_connect($ldapServer, $ldapPort) or die("Error: Unable to connect to LDAP server.");
    ldap_set_option($conn, LDAP_OPT_REFERRALS, 0);
    ldap_set_option($conn, LDAP_OPT_PROTOCOL_VERSION, 3);
    $bind = ldap_bind($conn, $dn, "812KKKlm102") or die("Error: Unable to bind to LDAP server.");

    if ($bind) {
      $res = ldap_search($conn, 'ou=loginer,dc=ldap,dc=test,dc=local', "cn=$username", array("ou"));
      if ($res) {
        $res = ldap_get_entries($conn, $res);
        error_log($res['0']['dn']);
        //die($res['count']);
        ldap_set_option($conn, LDAP_OPT_REFERRALS, 0);
        ldap_set_option($conn, LDAP_OPT_PROTOCOL_VERSION, 3);
        
        //return $res['count'];
        
        if (isset($res['0']) && isset($res['0']['dn']) && !empty($res['0']['dn'])) {
          $check = ldap_bind($conn, $res['0']['dn'], $pass);
          if ($check) {
            return true;
          }
        }
      }
    }
    return false;

  }

/** @noinspection PhpInconsistentReturnPointsInspection */
function check_auth_ldap () {
    return true;

  $sessionTimeoutSecs = 36000;
  
  if (!isset($_SESSION)) {
    session_start();
    //error_log("SESSION START: $_SESSION[username]");
  }
  if ($_SERVER['REQUEST_URI'] == "/logout.php") {
    error_log("LOGOUT");
      session_destroy();
      header('Location: login.php');
      return false;
  }

  if (!empty($_SESSION['lastactivity']) && $_SESSION['lastactivity'] >time()-$sessionTimeoutSecs) {
    error_log("SESSION AUTHED");
    // Session is already authenticated    
    if (check_ldap($_SESSION['username'], $_SESSION['password'])) {
      error_log($_SESSION['username']);
      $_SESSION['lastactivity'] = time();
      return true;
    } else {
      echo "Error ";
      session_destroy();
      header("Location: logout.php");
      //exit;
    }

  } else if (isset($_POST['username'], $_POST['password'])) {

    // Handle login requests    
    if (check_ldap($_POST['username'], $_POST['password'])) {
      // Successful auth
      $_SESSION['lastactivity'] = time();
      $_SESSION['username'] = $_POST['username'];
      $_SESSION['password'] = $_POST['password'];
      return true;
    } else {
      // Auth failed
      session_destroy();
      header("Location: logout.php");
      //exit;
    }

  } else if ($_SERVER['REQUEST_URI'] == "/login.php") {
    error_log("MSG");
    //header('Location: index.php');
    return false;
  } else {
    // Session has expired or a logout was requested
    session_destroy();
    header("Location: logout.php");
    //exit;

  }


}


