﻿select distinct count(res.disk_usage) over (partition by res.disk_usage), res.disk_usage from (
SELECT get_group((win_used.value / win_total.value) * 100) as disk_usage, win_used.hname as name from (
	select sum(avg_used.value) as value, avg_used.hid as hid, avg_used.name as hname from (
		select distinct i.itemid, i.hostid, avg(h.value) over (partition by i.itemid) as value,
			hs.name as name,
			hs.hostid as hid,
			i.key_ 
		from
			items i
			left join history_uint h on i.itemid = h.itemid
			left join hosts hs on i.hostid=hs.hostid
		where 
			i.name = 'Used disk space on $1'
			and lastvalue is not null
			and key_ like 'vfs\.fs\.size%\:%'
			and h.clock between extract(epoch from now())::bigint - 3600 AND extract(epoch from now())::bigint
		) as avg_used
		group by avg_used.hid, avg_used.name
	) as win_used
left join (
	select distinct on (sum(lastvalue::bigint) over (partition by key_ like 'vfs\.fs\.size%\:%')) 
		sum(lastvalue::bigint) over (partition by (key_ like 'vfs\.fs\.size%\:%')) as value,
		hostid
	from
		items 
	where 
		name = 'Total disk space on $1'
		and lastvalue is not null
		and key_ like 'vfs\.fs\.size%\:%'
) as win_total
on win_used.hid=win_total.hostid
UNION
-- linux disk usage
-- linux disk total
select 
	get_group((linux_used.value / linux_total.value::bigint) * 100) as disk_usage,
	linux_used.hname as name
from (
SELECT DISTINCT
	h.name, AVG(hst.value) over (partition by i.itemid) as value,
	h.hostid as hid,
	h.name as hname
FROM 
	history_uint hst 
	left join items i on i.itemid=hst.itemid 
	left join hosts h on h.hostid=i.hostid 
WHERE 
	hst.itemid in (
		select itemid
		from items 
		where name = 'Used disk space on $1' and lastvalue is not null and key_ like 'vfs\.fs\.size%/\,%'
	) 
	and hst.clock between extract(epoch from now())::bigint - 3600 AND extract(epoch from now())::bigint
) as linux_used
left join (
-- linux disk used	
select
	lastvalue as value, hostid as hid
from
	items
where
	name = 'Total disk space on $1'
	and lastvalue is not null
	and key_ like 'vfs\.fs\.size%/\,%'
) as linux_total
on linux_used.hid = linux_total.hid
) as res;