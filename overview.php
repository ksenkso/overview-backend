<?php 
	include 'auth.php';
	include 'psql.php';
	include 'mysql.php';
	
	$r = check_auth_ldap();

	if ($r) {	
		$response = array(
			'env' => array(),
			'avail' => array(),
			'perf' => array(
				'cpu' => array(),
				'mem' => array(),
				'disk' => array()
			)
		);


		/*$overview = array();
		$overview['env'] = array();
		$overview['avail'] = array();*/
		$response['perf'] = array();

		$c = p_connect();
		$sensors = get_sensors($c);
		$avail_servers = get_availability($c, 'Servers');
		$avail_network = get_availability($c, 'Network Devices');
		$avail_sensors = get_availability($c, 'Sensors');
		$availability_hardware = array();
		if ($avail_servers && $avail_network && $avail_sensors) {
			$availability_hardware = array($avail_servers, $avail_network, $avail_sensors);
		}
		
		$cpu = get_cpu($c);
		$mem = get_memory($c);
		$disk = get_disk($c);
		$response['env'] = $sensors;
		$response['perf']['cpu'] = $cpu;
		$response['perf']['mem'] = $mem;
		$response['perf']['disk'] = $disk;
		$response['avail']['hardware'] = $availability_hardware;
		$response['avail']['services'] = $availability_hardware;
		$response['avail']['inventory'] = $availability_hardware;

		echo json_encode($response);
	}
	
