
<!DOCTYPE html>
<html>
<head>
	<title>Hello!</title>
</head>
<body>
<?php 
include 'auth.php';
$c = check_auth_ldap();
?> 
 <div id="root"></div>
 
 <link rel="stylesheet" href="/css/app.css" >
 <script src="/js/d3.min.js"></script>
 <script src='/js/app.js' defer></script>
</body>
</html>