<?php 
	function m_connect() {
		$db = mysqli_init();
		mysqli_options($db, MYSQLI_OPT_SSL_VERIFY_SERVER_CERT, true);
		$db -> ssl_set('/usr/lib/ssl/certs/dashboard/client-key.pem', '/usr/lib/ssl/certs/dashboard/client-cert.pem', '/usr/lib/ssl/certs/dashboard/ca.pem', NULL, NULL);
		
		$db -> real_connect('127.0.0.1', 'ocsweb', 'ocspass','ocsweb', 3306, NULL, MYSQLI_CLIENT_SSL) or die("ERR: ". mysqli_connect_error());
		$db -> set_charset("utf8");
		return $db;
	}

	function m_query($db, $query_str) {
		/*$escaped = $db -> real_escape_string($query_str);
		echo $escaped."\n";*/
		$stmt = $db -> prepare($query_str);
		
		$stmt -> execute();
		$meta = $stmt->result_metadata(); 

		while ($field = $meta->fetch_field()) { 
		    $params[] = &$row[$field->name]; 
		} 

		call_user_func_array(array($stmt, "bind_result"), $params);    

		$hits = array();
		while ($stmt->fetch()) { 
			$c = array();
			foreach($row as $key => $val) { 
		        array_push($c, $val);
		    } 
		    array_push($hits, $c);
		} 
		
		$stmt -> close();
		$db -> close();
		return $hits;
	}
