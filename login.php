<?php 
	include 'auth.php';
	if (check_auth_ldap()) header('Location: index.php');
	
 ?>
<!DOCTYPE html>
<html>
<head>
	<title>Log in</title>
	<link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<body>
	<div class="page">
		<header class="header">
			<div class="logo"></div>
			<div class="header__title">Overview Dashboard</div>
		</header>
		<main class="content">
			<form class="form" method="POST" action="/index.php">
				<h3 class="form__title">Please, log in</h3>
				<div class="form__inputs">
					<input type="text" name="username" id="username" placeholder="login@domain.com">
					<input type="password" name="password" id="password" placeholder="Password">
				</div>
				<div class="button__container">
					<button type="submit">Log in</button>
				</div>
			</form>
		</main>
		<footer>
			<address>
				Hello World!
			</address>
		</footer>
	</div>
</body>
</html>