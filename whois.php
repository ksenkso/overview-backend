<?php 
include 'auth.php';
include 'mysql.php';

if (check_auth_ldap()) {

	$QUERY = "SELECT 
			h.name as name,
			n.ipaddress as ip,
			n.macaddr as MAC,
			h.userid as User,
			b.type as type,
			b.smanufacturer as manufacturer,
			b.smodel as model,
			h.osname as OS,
			c.type as CPU_type,
			c.current_speed as CPU_Freq,
			c.cores as CPU_Qty,
			h.memory as RAM
			from 
			hardware h
			left join networks n on h.id=n.hardware_id
			left join bios b on b.hardware_id=h.id
			left join cpus c on c.hardware_id=h.id";
	
	$db = m_connect();

	$filters = array();

	$filters['dn'] = !empty($_GET['Hostname']) && isset($_GET['Hostname']) ? "'%".$_GET['Hostname']."%'" : "'%'";
	$filters['ip'] = !empty($_GET['IP']) && isset($_GET['IP']) ? filter_input(INPUT_GET, 'IP', FILTER_VALIDATE_IP): "'%'";
	$filters['mac'] = !empty($_GET['MAC']) && isset($_GET['MAC']) ? $_GET['MAC'] : "'%'";
	$filters['user'] = !empty($_GET['User']) && isset($_GET['User']) ? "'%".$_GET['User']."%'" : "'%'";

	$QUERY .= " WHERE h.name ";
	$QUERY .= 'like '.$filters['dn'].' '; //isset($filters['dn']) && !empty($filters['dn']) ? "='".$filters['dn']."' " : "like '%' ";
	$QUERY .= "AND n.ipaddress like ".$filters['ip'].' ' ;
	//$QUERY .= isset($filters['ip']) && !empty($filters['ip']) ? "='".$filters['ip']."' " : "like '%' ";
	$QUERY .= "AND n.macaddr like ".$filters['mac'].' ' ;
	//$QUERY .= isset($filters['mac']) && !empty($filters['mac']) ? "='".$filters['mac']."'" : "like '%' ";
	$QUERY .= "AND h.userid like ".$filters['user']." ";
	$QUERY .= "GROUP BY(h.name)";
	error_log($QUERY."\n");

	$r = m_query(
		$db,
		$QUERY
		);
	echo json_encode($r);

}

 ?>